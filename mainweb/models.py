from django.db import models
from django.utils import timezone

class JadwalPribadi(models.Model):
    nama = models.CharField(max_length=255)
    tempat = models.CharField(max_length=255)
    kategori = models.CharField(max_length=255)
    tanggal = models.DateTimeField(blank=False, null=False)

    def __str__(self):
        return self.nama
