from django.shortcuts import render
from django.shortcuts import redirect
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from .forms import JadwalForm
from .models import JadwalPribadi
from django.http import HttpResponseRedirect

def index(request, something="none"):
	data = JadwalPribadi.objects.all()
	if(request.method == 'POST'):
		form = JadwalForm(request.POST or None)
		if(form.is_valid()):
			page = "form"
			response = {}
			response['nama'] = request.POST['nama']
			response['tempat'] = request.POST['tempat']
			response['kategori'] = request.POST['kategori']
			response['tanggal'] = request.POST['tanggal']
			jadwal = JadwalPribadi(nama = response['nama'],
				tempat = response['tempat'],
				kategori = response['kategori'],
				tanggal = response['tanggal'])
			jadwal.save()
			form = JadwalForm()
	else:
		form = JadwalForm()
		if(something == "delete"):
			page = "form"
			something = "none"
		else:
			page = "home"
	return render(request, 'index.html', {'jadwal_form' : form,
		'jadwal_data' : data, 'page' : page, 'req' : request.POST})

def deleteAll(request):
	JadwalPribadi.objects.all().delete()
	lastAct = "delete"
	return index(request, lastAct)
